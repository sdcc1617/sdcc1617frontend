
app.controller('assignCtrl', function ($scope,$rootScope, ResourceService,NgMap, $window,$route, $http) {

    $scope.signalations = []; //lista delle segnalazioni
    $scope.blank = " ";
    $scope.selectedOperator = "";
    $scope.operators = []; //lista degli operatori
    $scope.strToParse = "";
    $scope.startLongitude = 12.6273 ; //parametri per la visualizzazione della mappa
    $scope.startLatitude = 41.8577;
    $scope.startZoom = 5;
    $scope.title = ""; //parametri per la visualizzazione delle info window associate ai marker
    $scope.imgUrl = "";
    $scope.directory ="dir";
    $rootScope.springUrl = configData.springUrl;
    $rootScope.etcdUrl = configData.etcdUrl;



    $scope.markers = [];

    //Prendo l'elenco degli operatori dal backend
    $scope.getOperators = function () {
        ResourceService.getOp({
            role: "Operatore"},$rootScope.springUrl
        ).then(
            function (data) {
                $scope.operators = data;
            },
            function () {
                $scope.operators = [];
            });
    }
    //Prendo l'elenco delle segnalazioni aperte dal backend
    $scope.getSign = function () {
        ResourceService.getSignalations(null, $rootScope.etcdUrl //attenzione al fatto che qui passo un parametro vuoto
        ).then(
            function (data) {
                $scope.signalations = data;
            },
            function () {
                $scope.signalations = [];
            });
    }

    //creo un marker sulla mappa in base alla segnalazione scelta
    $scope.createPoi = function (data) {
        var vm = this;
        NgMap.getMap().then(function(map) {
            $rootScope.map = map;
            vm.showCustomMarker= function(evt) {
                map.customMarkers.foo.setVisible(true);
                map.customMarkers.foo.setPosition(this.getPosition());
            };
            vm.closeCustomMarker= function(evt) {
                this.style.display = 'none';
            };
        });
        $scope.selectedId = data.key.split("/")[2];
        $scope.selectedLongitude = data.value.split("|")[1];
        $scope.selectedLatitude = data.value.split("|")[2];
        $scope.startLongitude = $scope.selectedLongitude ;
        $scope.startLatitude = $scope.selectedLatitude;
        $scope.startZoom = 8;
        ResourceService.getInfo($scope.selectedLongitude,$scope.selectedLatitude, $rootScope.etcdUrl
        ).then(
            function (data) {
                $scope.title = data.title;
                $scope.imgUrl = data.image;
                $scope.message = data.message;
                $scope.topic = data.topic;

            },
            function () {
                $scope.info = [];
            });
    }
    //Assegna ad un operatore la segnalazione scelta e ricarica la pagina
    $scope.assign = function () {
        $scope.stringtoPass = $scope.directory+"/"+$scope.selectedOperator.email+"/"+$scope.selectedId;
        ResourceService.assignSignalation($scope.stringtoPass, $rootScope.etcdUrl).then(
            function () {
                alert("Assegnazione avvenuta con successo");
                $route.reload();
                $scope.whait = false;
            },
            function (response) {
                $scope.whait = false;
                alert("Errore nell'assegnazione, controlla di aver selezionato una segnalazione ed un operatore");
                $route.reload();
            });
    }

});
