app.controller('checkCtrl', function ($scope,$rootScope, ResourceService,NgMap, $window,$route) {

    $scope.signalations = []; //lista delle segnalazioni
    $scope.blank = " "; //carattere vuoto per la stampa
    $scope.selectedState = ""; // stato selezionato
    $scope.states = []; //lista dei possibili stati
    $scope.strToParse = "";
    $scope.startLongitude = 12.6273 ; //parametri di visualizzazione della mappa
    $scope.startLatitude = 41.8577;
    $scope.startZoom = 5;
    $scope.title = ""; //informazioni sulla segnalazione
    $scope.imgUrl = "";
    $scope.directory = "dir";
    $rootScope.springUrl = configData.springUrl;
    $rootScope.etcdUrl = configData.etcdUrl;



    $scope.markers = [];
    $scope.start = function () {
        var windowElement = angular.element($window);
        windowElement.on('beforeunload', function (event) {
            event.preventDefault();
            //below is the redirect part.
            $route.reload();

        });
    }
    //get degli stati dal backend
    $scope.getStates = function () {
        ResourceService.getAllStates(null,$rootScope.etcdUrl //attenzione qui primo parametro prima era vuoto
        ).then(
            function (data) {
                $scope.states = data;
            },
            function () {
                $scope.states = [];
            });
    }
    //get delle segnalazioni dal backend
    $scope.getSign = function () {
        ResourceService.getSignalationsOp($rootScope.user.email,$rootScope.etcdUrl
        ).then(
            function (data) {
                $scope.signalations = data;
            },
            function () {
                $scope.signalations = [];
            });
    }

    //crea un marker sulla mappa in base alla segnalazione scelta dall'utente
    $scope.createPoi = function (data) {
        var vm = this;
        NgMap.getMap().then(function(map) {
            $rootScope.map = map;
            vm.showCustomMarker= function(evt) {
                map.customMarkers.foo.setVisible(true);
                map.customMarkers.foo.setPosition(this.getPosition());
            };
            vm.closeCustomMarker= function(evt) {
                this.style.display = 'none';
            };
        });
        $scope.selectedId = data.key.split("/")[2];
        $scope.currentState = data.value.split("|")[0];
        $scope.selectedLongitude = data.value.split("|")[1];
        $scope.selectedLatitude = data.value.split("|")[2];
        $scope.startLongitude = $scope.selectedLongitude ;
        $scope.startLatitude = $scope.selectedLatitude;
        $scope.startZoom = 8;
        ResourceService.getInfo($scope.selectedLongitude,$scope.selectedLatitude, $rootScope.etcdUrl
        ).then(
            function (data) {
                $scope.title = data.title;
                $scope.imgUrl = data.image;
                $scope.message = data.message;
                $scope.topic = data.topic;

            },
            function () {
                $scope.info = [];
            });
    }
    //al clic sul bottone assegna lo stato alla segnalazione
    $scope.assignState = function () {
        if($scope.selectedState.value =="DONE"){
            $scope.stringtoPass = $scope.selectedLongitude+"/"+$scope.selectedLatitude+"/"+$rootScope.user.email+"/"+$scope.selectedId;
            ResourceService.completedSignalation($scope.stringtoPass,$rootScope.etcdUrl).then(
                function () {
                    alert("Completamento della segnalazione avvenuto con successo");
                    $route.reload();
                    $scope.whait = false;
                },
                function () {
                    alert("Errore nel completamento della segnalazione");
                    $route.reload();
                    $scope.whait = false;
                });
        }
        else {
            $scope.stringtoPass = $scope.selectedState.value + "/" + $rootScope.user.email + "/" + $scope.selectedId;
            ResourceService.changeSignalationState($scope.stringtoPass,$rootScope.etcdUrl).then(
                function () {
                    alert("Cambio stato avvenuto con successo");
                    $route.reload();
                    $scope.whait = false;
                },
                function () {
                    $scope.whait = false;
                    alert("Errore nel cambio stato, controlla di aver selezionato una segnalazione ed uno stato");
                    $route.reload();
                });
        }
    }

});

