app.controller('loginCtrl', function ($scope,$rootScope, ResourceService, $window,$route) {

    $scope.whait = false;
    $rootScope.springUrl = configData.springUrl;
    $rootScope.etcdUrl = configData.etcdUrl;


    $scope.login = function () {
        $scope.whait = true;
        return ResourceService.checkUser({
            email: $scope.email,
            password: $scope.password
        },$rootScope.springUrl).then(
            function (data) {
                $rootScope.user = data;
                alert("Login avvenuto con successo");
                $rootScope.isLogged = true;
                if ($rootScope.user.role == "Amministratore"){
                    $rootScope.isAd = true;
                    $window.location.href = '#!/assignSignals';
                }
                else if ($rootScope.user.role == "Operatore"){
                    $window.location.href = '#!/checkSignals';
                    $rootScope.isOp = true;
                }
                $scope.whait = false;
            },
            function () {
                $scope.whait = false;
                alert("Errore nel login, controllare email e password");
                $rootScope.isLogged = false;
                $rootScope.user.name="Utente";
            });
    }
});
