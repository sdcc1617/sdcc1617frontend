app.controller("indexController", function ($scope,$rootScope,$window, UserService, ResourceService) {
    $rootScope.isLogged = false;
    $rootScope.springUrl = configData.springUrl;
    $rootScope.etcdUrl = configData.etcdUrl;

    $scope.logout = function () {
        $rootScope.isLogged = false;
        $window.location.href = '#!/login';
        $rootScope.isLogged = false;
        $rootScope.isAd = false;
        $rootScope.isOp = false;
    };


});


