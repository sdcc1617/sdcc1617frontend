app.controller("homeCtrl", function ($scope, ResourceService, UserService) {

    $scope.result1 = [];
    $scope.result2 = [];
    $scope.result3 = [];
    $scope.result4 = [];
    $scope.user = "";

    //Prodotti
    $scope.index1 = "Costo";
    $scope.index2 = "Disponibile";
    //Piatti
    $scope.index3 = "Costo";
    $scope.index4 = "Guadagno";

    $scope.addResult1 = function () {
        return ResourceService.getTopThreeProduct({
            index: $scope.index1
        })
            .then(
                function (data) {
                    $scope.result1.push("1. " + data.first);
                    $scope.result1.push("2. " + data.second);
                    $scope.result1.push("3. " + data.third);
                },
                function (err) {
                    $scope.result1.push("Dati non disponibili");
                });
    };

    $scope.addResult2 = function () {
        return ResourceService.getTopThreeProduct2({
            index: $scope.index2
        })
            .then(
                function (data) {
                    $scope.result2.push("1. " + data.first);
                    $scope.result2.push("2. " + data.second);
                    $scope.result2.push("3. " + data.third);
                },
                function (err) {
                    $scope.result2.push("Dati non disponibili");
                });
    };

    $scope.addResult3 = function () {
        return ResourceService.getTopThreeFood({
            index: $scope.index3
        })
            .then(
                function (data) {
                    $scope.result3.push("1. " + data.first);
                    $scope.result3.push("2. " + data.second);
                    $scope.result3.push("3. " + data.third);
                },
                function (err) {
                    $scope.result3.push("Dati non disponibili");
                });
    };

    $scope.addResult4 = function () {
        $scope.result4.push("Dati non disponibili");
    };


    $scope.lastReports = [];
    var u =  UserService.getUser();
    if (angular.equals(u,UserService.getTM()))
        $scope.user = "reportsTM";
    else if(angular.equals(u,UserService.getMC()))
        $scope.user = "reportsMC";
    else
        $scope.user = "reportsML";
    $scope.getLastReport = function () {
        return ResourceService.getLastReports({
            utente: UserService.getUser()
        }).then(function (data) {
                var currentdate = new Date();
                $scope.datetime = "Ultimo aggiornamento:\t" + currentdate.getDate() + "-"
                    + (currentdate.getMonth() + 1) + "-"
                    + currentdate.getFullYear() + " @ "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();
                $scope.lastReports = data;
            },
            function (err) {
                var currentdate = new Date();
                $scope.datetime = "Ultimo aggiornamento:\t" + currentdate.getDate() + "-"
                    + (currentdate.getMonth() + 1) + "-"
                    + currentdate.getFullYear() + " @ "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();
            });
    };

});
