app.controller('registerCtrl', function ($scope, ResourceService, $rootScope, $window) {

    $scope.roles = ["Operatore","Amministratore"];
    $scope.selectedRole = "";
    $scope.whait = false;
    $rootScope.springUrl = configData.springUrl;
    $rootScope.etcdUrl = configData.etcdUrl;


    $scope.register = function () {

        if (!$scope.name ||
            !$scope.surname ||
            !$scope.email ||
            !$scope.password ||
            !$scope.selectedRole){
            $scope.whait = false;
            alert("Errore, sono presenti campi vuoti");
        }
        else{
        /*if (!angular.equals($scope.parenthesis, 0)) {
         alert("Errore inserimento indice, controllare le parentesi");
         return;
         }
         if ($scope.indName === undefined || $scope.indName === null || angular.equals($scope.indName, "")) {
         alert("Inserire un nome per il nuovo indice");
         return;
         }
         if ($scope.indDescr === undefined || $scope.indDescr === null || angular.equals($scope.indDescr, "")) {
         alert("Inserire una descrizione per il nuovo indice");
         return;
         }
         if (angular.equals($scope.textIndex, "")) {
         alert("Inserire una formula per il nuovo indice");
         return;
         }*/
        $scope.whait = true;
        ResourceService.saveUser({
            name: $scope.name,
            surname: $scope.surname,
            password: $scope.password,
            email: $scope.email,
            role: $scope.selectedRole
        },$rootScope.springUrl).then(
            function () {
                alert("Registrazione avvenuta con successo");
                $window.location.href = '#!/login';
                $scope.whait = false;
            },
            function () {
                $scope.whait = false;
                alert("Errore nella registrazione, controllare i dati");
            });
        }
    }





});
