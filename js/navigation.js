var app = angular.module("myApp", ["ngRoute", "ui.select2", 'ngMap','ui.select', 'ngSanitize']);





//definizione dell'app e delle route, per muoversi tra le view dell'applicazione


app.config(function ($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "Views/viewHome/login.html"
        })
        .when("/login", {
            templateUrl: "Views/viewHome/login.html"
        })
        .when("/register", {
            templateUrl: "Views/viewHome/register.html"
        })
        .when("/checkSignals", {
            templateUrl: "Views/checkSignals.html"
        })
        .when("/assignSignals", {
            templateUrl: "Views/assignSignals.html"
        })
});