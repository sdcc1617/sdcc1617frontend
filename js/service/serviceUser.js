app.service('UserService', function () {
    var user = 0;

    this.setUser = function (data) {
        user = data;
    };
    this.getUser = function () {
        return user;
    };
    this.getTM = function () {
        return "Top Manager";
    };
    this.getMC = function () {
        return "Manager Centrale";
    };
    this.getML = function () {
        return "Manager Locale";
    };
    this.getMagCen = function () {
        return "Magazzino Centrale";
    };
    this.getRmCn = function () {
        return "Roma Centro";
    };

});
