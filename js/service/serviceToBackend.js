app.service("ResourceService", function ($q, $http, $rootScope) {

    //funzione generale che realizza chiamate http per la comunicazione con il backend, vengoni passati sempre json
    var ajax = function (method, url, data) {

        var deferred = $q.defer();
        var request = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        if (method === 'GET' || method === 'DELETE') {
            request.params = data;
        } else {
            request.data = data;
        }
        $http(request).then(function (response) {
            deferred.resolve(response.data);
        }, function (response, status) {
            deferred.reject({data: response, status: status});
        });
        return deferred.promise;
    };

    this.saveUser = function (param,url) {
        return ajax("POST",url+"user/createUser",param);
    };
    this.changeSignalationState = function (param,url) {
        return ajax("POST",url+"changeSignalationState/"+param);
    };
    this.completedSignalation = function (param,url) {
        return ajax("POST",url+"completedSignalation/"+param);
    };
    this.assignSignalation = function (param,url) {
        return ajax("POST",url+"reassignSignalationInEtcd/"+param);
    };
    this.getInfo = function (long,lat,url) {
        return ajax("GET",url+"getSignalationFromDynamoDB/"+long+"/"+lat);
    };
    this.checkUser = function (param,url) {
        return ajax("POST",url+"user/isUser",param);
    };
    this.getAllStates = function (param,url) {
        return ajax("GET",url+"getAllPossibleSignalationStates",param);
    };
    this.getOp = function (param,url) {
        return ajax("GET",url+"user/getUsersByRole",param);
    };
    this.getSignalations = function (param,url) {
        return ajax("GET",url+"getSignalationsFromEtcd/dir",param);
    };
    this.getSignalationsOp = function (param,url) {
        return ajax("GET",url+"getSignalationsFromEtcd/"+param);
    };
    this.getHello = function (param) {
        return ajax("POST",$rootScope.springUrl+"/user/getHello",param);
    };

});